#!/usr/bin/python
# encoding: GBK
#
# -----------------------------------------------------------------------------
# lex_test.py
#
#   unit test for lex file
#   
# -----------------------------------------------------------------------------

import sys
import unittest

sys.path.insert(0,"../clang")
from clex import *

data1 = """
#include <include.h>
int b;
int a=1;
int b=(a);
/** abcd */
/** abcd */
int func(int b)
{
	/** step1*/
	a=2;
	/** judge a */
	if (a==2){	
		/** do left */
	}else
	{
		/** do right */
	}
}
"""

class test_lex(unittest.TestCase):
    """
    @brief test lex part
    """
    def test_token(self):
        lex.runmain(data=data1)
        print "TEST result for lex"
        print cglobal.comment_map

if __name__ == '__main__':
	unittest.main()
