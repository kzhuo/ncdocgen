#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------------
# tvisio.py
#

from cvisio import *

TV_ID=0
TV_X=1
TV_Y=2
TV_TYPE=3
TV_TEXT=4
TV_LINE=5

def get_conn_text(node1):
        try:
            t1 = "[%2d]"%node1[TV_ID]
        except:
            if isinstance(node1, int):
                t1 = "%2d"%node1
            elif node1 is None:
                t1 = "nil"
        return t1

class tvisio(cvisio):
    def __init__(self, filename, autoquit=0):
        self.end_list = []
        self.count = 0
        print("tvisio: "+filename)

    def get_id(self):
        self.count+=1
        return self.count

    def open(self, name=''):
        print("tvisio open "+name)

    def new_page(self, name):
        pass

    def draw_node(self, type, x, y, text, line=0):
        count = self.get_id()
        print("+++", "[%2d]"%count, y, x, get_node_name(type), text,line)
        return [count, x, y, type, text, line]

    def conn_node(self, node1, node2, type=CONN_P2P, text='', width=0):
        t1 = get_conn_text(node1)
        t2 = get_conn_text(node2)

        print("---", "    ", t1, "to", t2, text)

    def copy2sys(self):
        print("tvisio copy 2 sys")

    def close(self):
        print("tvisio close")
