# -*- coding: utf-8 -*-
from distutils.core import setup
import glob, py2exe

from cdocgen import __version__

# py -2 setup.py pye2xe

includes = ["encodings", "encodings.*"] 
options = {"py2exe": 
            {   "compressed": 1, 
                "includes": includes, 
                #"bundle_files": 1 
            } 
          } 

setup(
    name = "cdocgen",
    version = __version__,
    options = options,
    console=['cdocgen.py'],
    data_files = [
        ("user", glob.glob("user\\*.doc")),
        ("txt", glob.glob("txt\\*.c")),
    ]
     )

