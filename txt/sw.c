int32_t set_ptr_value(void *dst, int32_t data, int8_t width)
{
	switch (width)
	{
		case  1: 
			*(uint8_t *)dst = data;
			break;
		case  2: 
			*(uint16_t*)dst = data;
			break;
		case  4: 
			*(uint32_t*)dst = data;
			break;
		case -1: 
			*( int8_t *)dst = data;
			break;
		case -2: 
			*( int16_t*)dst = data;
			break;
		case -4: 
			*( int32_t*)dst = data;
			break;
		default: 
			data = 0;
			break;
	}
	return get_ptr_value(dst, width);
}

