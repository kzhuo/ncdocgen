/** 
 * @brief sync_adjust 本函数用于进行交叉点的校正
 * 
 * @param dis 距离
 * @param type 类型
 * 
 * @return 返回校正的索引数
 */
int16_t sync_adjust(uint16_t dis, int16_t type)
{
	int16_t index;
	/** 所收到的有效交叉点组的计数自增 */
	_sync_count++;	
	/** 类型有效*/
	if (type >=0 )
	{	
		/** 获取交叉点组编号 */
		index = sync_get_index(type, count - last_sync_dis, NULL);

		/** 如果是第一组交叉点组*/
		if (sync_count() == 1)
		{
			/** 更新最新的交叉点组的距离*/
			last_sync_dis = dis;	
			index = -1;
		}else{;}
		   
		if (index != -1){
			last_sync_dis = dis;	
		}else{;}

		//haha();
	}else{
	   index = -1;
	}
	return index;
}
