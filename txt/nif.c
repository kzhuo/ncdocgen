fi()
{
	if (src >= 1<<8){
		if (src >= 1<<12){
			if (src >= 1<<14){
				return 14;
			}else{
				return 12;
			}
		}else{
			if (src >= 1<<10){
				return 10;
			}else{
				return 8;
			}
		}
	}else{
		if (src >= 1<<4){
			if (src >= 1<<6){
				return 6;
			}else{
				return 4;
			}
		}else{
			if (src >= 1<<2){
				return 2;
			}else{
				return 0;
			}
		}
	}
}
