
/**
* @brief f1 进行switch的简单测试
* @param a 为输入的一个参数
* @detail 包含主要的break分支，包含一个不包含break的分支
*/
void f1(int a)
{
	if (a){
		/** sw */
		switch(e1)
		{
			/** case e2*/
			case e2:
				/** dodo e2*/
				if (1){
					go();
				}else{
					dodo();
				}
				/** 可能 */
				go2();
				go3();
				/* break e2*/
				break;

			case e2:
			case e3:
				if (dodo())
				{
					dodo1();
				}
				break;
			case tt:
				break;
		}
	}

	/** gogo*/
	asgg();

}
