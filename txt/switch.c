
/**
* @brief f1 进行switch的简单测试
* @param a 为输入的一个参数
* @detail 包含主要的break分支，包含一个不包含break的分支
*/
void f1(int a)
{
	if (a){
		/** sw */
		switch(e1)
		{
			/** case e2*/
			case e2:
				/** dodo e2*/
				go();
				/* break e2*/
				break;

			case e3:
				/** e.3*/
				go();
				/* break e3*/
				break;

			case e32:
				/** e.32*/
				go();
				/* break e3 2*/


			case e4:
				/** e.4*/
				go();
				/* break e4*/
				return ;

			case 55:
				if (1)
				{
					do_1();
				}else{
					sdf();
				}

				//		/** default*/
				//		default:
				//			/** defl*/
				//			a();
		}
	}

	/** gogo*/
	asgg();

}
