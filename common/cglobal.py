#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------------
# condraw.py
#
# 

import sys

# 用于存储中间数据的MAP的索引
MAP_CODE =0
MAP_CM	 =1
MAP_IDEN =2

VTYPE_GLOBAL=0
VTYPE_PARAM=1
VTYPE_LOCAL=2
VTYPE_FUN=3

# C for chart, 用于生成流程图的相关文件
CHART_LINE=0
CHART_TYPE=1	#
CHART_TOKEN=2
CHART_NUM=3

# 数据还是类型
TOKENDATA_TYPE=0
TOKENDATA_LINK=1

# VNODE定义
VNODE_WIDTH=0
VNODE_HEIGHT=1
VNODE_ENTRY=2
VNODE_EXIT=3
VNODE_TYPE=4
VNODE_DATA=5

VNODE_PAIR_TYPE=0
VNODE_PAIR_NODE=1

#all_map=[{},{},{},{}]
#comment_map=all_map[MAP_CM]
##cpcomment_map=all_map[MAP_CP]
#key_map=all_map[MAP_CODE]
#ident_map=all_map[MAP_IDEN]

# 需要在具有选择时，需要考虑同一行的多种属性
# 定义TOKEN中，不同位置所存储的内容
TK_TYPE	=0;
TK_COUNT=1;
TK_START=1;
TK_END	=2;
TK_EXPR	=3;
TK_EXPR0=4;	# FOR
TK_EXPR2=5;	# FOR
TK_MID	=4;	# else

TK_EXPRS=4	# 对于单行，且不包含{}的statement

DEFAULT_DEPTH=10

USELIST=0    # 使用string还是list进行整合

# 打印的格式化字符
CM_FMTSTR=' %-10s|  '
CM_SEP='-'*70

FUN_SEP='#'*70
FUN_BLANK='-'*4
FUN_LFMT='%04d'
FUN_DELIM='  '

FUN_STR_WIDTH=28    # 用于调整标识符
FUN_STR_FMT='%-27s '    # 用于注释换行

from config import *

# 用于轮询函数头部的列表
comment_seq_list = ['proto', 'brief', 'param', 'return', 'fcall', 'gvarin', 'gvarout', 'details']

def get_rcur_info():
	"""Return the frame object for the caller's stack frame."""
	try:
		raise Exception
	except:
		f = sys.exc_info()[2].tb_frame.f_back
	print('########### ', f.f_code.co_name, f.f_lineno, '#####')

DEBUG=0

def get_cur_info():
    if DEBUG==1:
        return "fun"
    else:
        """Return the frame object for the caller's stack frame."""
        try:
            raise Exception
        except:
            f = sys.exc_info()[2].tb_frame.f_back
        return (f.f_code.co_name, f.f_lineno)

# 更新
def updatestr(p, trim=0, sep=' '):
    p[0]=''
    for i in range(1, len(p) - trim):
        if (p[i]):
            p[0]+=p[i]
            if (p[0][-1] != ' '):
                #p[0] += ' '
                p[0] += sep

def print_com_err(err, lineinfo=""):
    print "com err %s"%lineinfo,
    for l in err:
        if (isinstance(l, str)):
            print l,
        elif (isinstance(l, tuple)):
            for w in l:
                if (isinstance(w, str)):
                    print w,
                else:
                    print w,
                    #for text in w.split():
        else:
            print l


import os

# 获取完整文件名
def get_full_name(name):
    if (name.find(':') == -1):
        #filename = os.path.dirname(sys.argv[0])+"\\"+name
        # 获取当前路径
        #filename = os.getcwd()+"\\"+name
        filename = os.getcwd().decode('gbk')+"\\"+name.decode('gbk')
        #print filename
    else:
        filename = name
    return filename

def get_short_doc_name(name):
    fslice = name.split('\\')
    if (len(fslice) >= 2):
        return '_CDOC_'+fslice[-2]+'_'+fslice[-1]
    else:
        return '_CDOC_'+fslice[0]

##############
data = '''
#include <include.h>
int b;
int a=1;
int b=(a);
/** abcd */
/** abcd */
int func(int b)
{
	/** step1*/
	a=2;
	/** judge a */
	if (a==2){	
		/** do left */
	}else
	{
		/** do right */
	}
}
'''
