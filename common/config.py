#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------------

comment_translate_table={
    # 对文件而言
    'file'	 : u'文    件', 
    'author' : u'作    者', 
    'date'	 : u'日    期', 
    # 对函数 而言
    'proto'	 : u'原    型', 
    'brief'	 : u'概    述', 
    'param'	 : u'参    数', 
    'return' : u'返 回 值', 
    'fcall'  : u'引    用',
    'gvarin' : u'输入全局',
    'gvarout': u'输出全局',
    'details': u'说    明',
    'depth'	 : u'深    度', 
    # 其他
    'alias'	 : u'别    名',
}

# 头部为空时的默认内容
comment_null_table={
    'brief'	 : u'函数%s的说明', 
    'param'	 : u'无', 
    'return' : u'无', 
    'fcall'  : u'无',
    'gvarin' : u'无',
    'gvarout': u'无',
    'details': u'无特别说明',
    'ignore' : u'y',
}

