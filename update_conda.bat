rd /s /q dist
python -c "import cdocgen; print cdocgen.main_version.split('-')[0]," > mainver.txt
python setup.py py2exe
mkdir nsisbuild || del /q nsisbuild\*
cd nsisbuild && cmake -G"MinGW Makefiles" .. && cmake --build . && cpack
cd ..
